var express = require('express');
var router = express.Router();
var esClient = require('../server/es.client');

/* GET newssearch page. */
router.get('/', (req, res) => {
    //res.render('index', { title: 'News Search' });
    searchAll(req, res);
    //await res.send(search);
    //res.send(searchByContents('아버지'));
});

router.post('/', (req, res) => {
    //const search_kor = req;
    //console.log("receive kor");
    console.log("검색어 : " + req.body.keyword_kor);
    searchByContents(req.body.keyword_kor, res);
});

function checkCluster() {
    esClient.cluster.health({}, function (err, resp, status) {
        console.log('-- client Health --', resp);
    });
}

function searchByContents(req, res) {
    esClient.search({
        index: 'news',
        size: 50,
        body: {
            query: {
                match_phrase_prefix: { 'kor': req }
            }
        }
    }, function (error, response, status) {
        if (error) {
            console.log('search error: ' + error);
        } else {
            //console.log('--- Response ---');
            //console.log(response);
            //console.log('--- Hits ---');
            //console.log('검색어 : ' + req);
            //console.log('----------------------------------------------------------------------------------------------------');
            if (response.body.hits.hits.length) {
                response.body.hits.hits.forEach((elem, index) => {
                    /*
                    console.log('[' + (index + 1) + ']');
                    console.log('url : ' + elem._source.URL);
                    console.log('날짜 : ' + elem._source.date);
                    console.log('언론사 : ' + elem._source.press);
                    console.log('분류 : ' + elem._source.auto_classification_1);
                    console.log('본문(한글) : ' + elem._source.kor);
                    console.log('본문(영어) : ' + elem._source.eng);
                    console.log('----------------------------------------------------------------------------------------------------');
                    */
                    //console.log(hit._source.kor);return elem._source.kor;
                })
            } else {
                console.log('No search results found!')
            }
            console.log('Send data from express to react');
            res.send(response.body.hits.hits);
        }
    });
}

async function searchAll(req, res) {
    const { body } = await esClient.search({
        index: 'news',
        size: 50,
        body: {
            query: {
                match_all: {}
            }
        }
    });
    //console.log(body.hits.hits);
    res.send(body.hits.hits);
}

module.exports = router;
