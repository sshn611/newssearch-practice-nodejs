const client = require('./es.client.js');
const elasticSearchSchema = require('./es.schema');

client.ping({
    requestTimeout: 3000,
}, function(error) {
    error
        ? console.error('ElasticSearch cluster is down!')
        : console.log('ElasticSearch is ok')
});

function ElasticSearchClient(body) {
    return client.search({index: 'news3', body: body});
}

function ApiElasticSearchClient(req, res) {
    ElasticSearchClient({...elasticSearchSchema}) // 왜 ... 들어가지?
        .then(r => res.send(r['hits']['hits']))
        .catch(e => {
            console.error(e);
            res.send([]);
        });
}

module.exports = {
    ApiElasticSearchClient,
    ElasticSearchClient
};