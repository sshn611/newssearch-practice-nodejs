import React from 'react';
import { HashRouter, Route } from "react-router-dom"
import Search from "./routes/Search";
import Navigation from "./component/Navigation";

function App() {
    return (
        <HashRouter>
            <Navigation/>
            <Route path="/" exact={true} component={Search} />
        </HashRouter>
    )
}

export default App;
