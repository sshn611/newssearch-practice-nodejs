import React from "react"
import PropTypes from "prop-types";

function News({id, press, classification, date, url, kor, eng}) {
    return (
        <div className="news">
            <p>신문사 : {press}</p>
            <p>분류 : {classification}</p>
            <p>날짜 : {date}</p>
            <p>URL : {url}</p>
            <p>kor : {kor}</p>
            <p>eng : {eng}</p>
            <br/><br/>
        </div>
    );
}

News.propTypes = {
    id: PropTypes.string.isRequired,
    press: PropTypes.string.isRequired,
    classification: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    kor: PropTypes.string.isRequired,
    eng: PropTypes.string.isRequired
};

export default News;