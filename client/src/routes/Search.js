import React, {Component} from 'react';
import axios from "axios";
import "./Search.css";
import News from "../component/News";

class Search extends Component {

    state = {
        isLoading: false,
        newsList: [],
        keyword_kor: ''
    };

    getNews = async() => {
        const { data } = await axios.get("http://localhost:9000/search");
        await this.setState({ newsList: data, isLoading: false }); // await 안 써도 돼?
        console.log(data);
    };

    getNewsFromKeyword = async() => {
        const { keyword_kor } = this.state;
        console.log("검색어 : " + keyword_kor);

        this.setState({ isLoading: true });
        return await axios.post('http://localhost:9000/search',
            {
                "keyword_kor": keyword_kor
            })
            .then(res => {
                this.setState({ newsList: res.data, isLoading: false });
                return res;
            });
    };

    componentDidMount() {
        this.getNews();
    }


    renderNews() {
        const { newsList } = this.state;
        return (
            newsList.map((news,index) => {
                return (
                    <News
                        key={news._source.id}
                        id={news._source.id}
                        press={news._source.press}
                        classification={news._source.auto_classification_1}
                        date={news._source.date}
                        url={news._source.URL}
                        kor={news._source.kor}
                        eng={news._source.eng}/>
                )
            })
        );
    }

    // handleInputChange(e) 랑 뭐가 다른지 확인
    handleInputChange = e => {
        //1. 아래처럼 하나하나 할 수도 있고, 혹은 [e.target.name]을 쓰면 일일이 추가 하지 않아도 해당 input의 name과 일치하는 state 안의 변수를 변경할 수 있어 한 번에 관리 가능하다.
        //* [ ] 의 쓰임새 제대로 정리하
        //this.setState({ search_kor: e.target.value });
        this.setState({ [e.target.name]: e.target.value });
    };

    // handleSubmit(e) 랑 뭐가 다른지 확인
    handleSubmit = e => {
        e.preventDefault();

        console.log("submit");

        //const { search_kor } = this.state;
        //2. 아래처럼 여기 submit 핸들러에서도 id로 조회해서 받아 쓸 수 있다.
        //const keyword_kor = document.getElementById('keyword_kor').value;

        this.getNewsFromKeyword()
            .then(res => console.log(res));
        /*
        axios
            .post('/', search_kor)
            .then(() => console.log('Success : send search_kor from react to express'))
            .catch(err => {
                console.error(err);
            });

         */
    };

    render() {
        const { isLoading } = this.state;
        return (
            <section className="container">
                <p>뉴스 검색</p>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            className="form-control"
                            //id="keyword_kor"
                            name="keyword_kor"
                            onChange={this.handleInputChange}
                        />
                    </div>
                </form>

                { isLoading ? (
                    <div className="loader">
                        <span className="loader_text">News Loading...</span>
                    </div>
                ) : (
                    <div className="newsList">
                        { this.renderNews() }
                    </div>
                )}

            </section>
        )
    }
}

export default Search;
